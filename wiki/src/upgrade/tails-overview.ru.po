# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: tails-l10n@boum.org\n"
"POT-Creation-Date: 2019-02-22 19:05+0000\n"
"PO-Revision-Date: 2018-10-25 10:37+0000\n"
"Last-Translator: emmapeel <emma.peel@riseup.net>\n"
"Language-Team: Russian <http://translate.tails.boum.org/projects/tails/"
"upgrade-tails-overview/ru/>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && n"
"%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Weblate 2.19.1\n"

#. type: Content of: outside any tag (error?)
#, fuzzy
msgid ""
"[[!meta title=\"Manually upgrade inside Tails (or Linux)\"]] [[!meta robots="
"\"noindex\"]] [[!meta stylesheet=\"bootstrap.min\" rel=\"stylesheet\" title="
"\"\"]] [[!meta stylesheet=\"install/inc/stylesheets/assistant\" rel="
"\"stylesheet\" title=\"\"]] [[!meta stylesheet=\"install/inc/stylesheets/"
"overview\" rel=\"stylesheet\" title=\"\"]] [[!meta stylesheet=\"install/inc/"
"stylesheets/upgrade-tails\" rel=\"stylesheet\" title=\"\"]] [[!inline pages="
"\"install/inc/overview\" raw=\"yes\" sort=\"age\"]] [["
msgstr ""
"[[!meta title=\"Manually upgrade inside Tails\"]] [[!meta robots=\"noindex"
"\"]] [[!meta stylesheet=\"bootstrap.min\" rel=\"stylesheet\" title=\"\"]] [[!"
"meta stylesheet=\"install/inc/stylesheets/assistant\" rel=\"stylesheet\" "
"title=\"\"]] [[!meta stylesheet=\"install/inc/stylesheets/overview\" rel="
"\"stylesheet\" title=\"\"]] [[!meta stylesheet=\"install/inc/stylesheets/"
"upgrade-tails\" rel=\"stylesheet\" title=\"\"]] [[!inline pages=\"install/"
"inc/overview.ru\" raw=\"yes\" sort=\"age\"]] [["

#. type: Content of: <div><div>
msgid "Let's go!"
msgstr ""

#. type: Content of: outside any tag (error?)
msgid "|upgrade/tails-download]]"
msgstr ""
