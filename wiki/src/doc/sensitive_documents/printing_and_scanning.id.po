# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: tails-l10n@boum.org\n"
"POT-Creation-Date: 2016-02-08 20:33+0100\n"
"PO-Revision-Date: 2018-11-13 01:46+0000\n"
"Last-Translator: emmapeel <emma.peel@riseup.net>\n"
"Language-Team: \n"
"Language: id\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 2.19.1\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"Printing and scanning\"]]\n"
msgstr ""

#. type: Title =
#, no-wrap
msgid "Printing\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"To configure a printer or manage your printing jobs choose\n"
"<span class=\"menuchoice\">\n"
"  <span class=\"guimenu\">Applications</span>&nbsp;▸\n"
"  <span class=\"guisubmenu\">System Tools</span>&nbsp;▸\n"
"  <span class=\"guimenuitem\">Settings</span></span>&nbsp;▸\n"
"  <span class=\"button\">Printers</span></span>.\n"
msgstr ""

#. type: Plain text
msgid ""
"To check the compatibility of your printer with Linux and Tails, consult the "
"[OpenPrinting database](https://www.openprinting.org/printers) of the Linux "
"Foundation."
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"<div class=\"tip\">\n"
"To reuse the configuration of the printers across separate working\n"
"sessions, you can activate the [[<span class=\"guilabel\">Printers</span>\n"
"persistence\n"
"feature|doc/first_steps/persistence/configure/#printers]].\n"
"</div>\n"
msgstr ""

#. type: Title =
#, no-wrap
msgid "Scanning\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"Tails includes <span class=\"application\">[Simple\n"
"Scan](https://launchpad.net/simple-scan)</span>, a tool to scan both\n"
"documents and photos.\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"To start <span class=\"application\">Simple Scan</span> choose\n"
"<span class=\"menuchoice\">\n"
"  <span class=\"guimenu\">Applications</span>&nbsp;▸\n"
"  <span class=\"guisubmenu\">Graphics</span>&nbsp;▸\n"
"  <span class=\"guimenuitem\">Simple Scan</span></span>.\n"
msgstr ""
