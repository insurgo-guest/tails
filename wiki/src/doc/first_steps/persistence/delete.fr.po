# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"POT-Creation-Date: 2017-07-31 21:51+0000\n"
"PO-Revision-Date: 2017-08-13 11:35+0000\n"
"Last-Translator: AtomiKe <tails@atomike.ninja>\n"
"Language-Team: \n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.8.11\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"Delete the persistent volume\"]]\n"
msgstr "[[!meta title=\"Supprimer le volume persistant\"]]\n"

#. type: Bullet: '  1. '
msgid ""
"Start Tails from the USB stick on which you want to delete the persistent "
"volume."
msgstr ""
"Démarrez Tails depuis la clé USB dont vous voulez supprimer le volume "
"persistant."

#. type: Plain text
#, no-wrap
msgid "     Do not enable the persistent volume in <span class=\"application\">Tails Greeter</span>.\n"
msgstr "     N'activez pas le volume persistant dans le <span class=\"application\">Tails Greeter</span>.\n"

#. type: Bullet: '  1. '
msgid ""
"Choose <span class=\"menuchoice\"> <span class=\"guimenu\">Applications</"
"span>&nbsp;▸ <span class=\"guisubmenu\">Tails</span>&nbsp;▸ <span class="
"\"guimenuitem\">Delete persistent volume</span></span>."
msgstr ""
"Choisissez <span class=\"menuchoice\"> <span class=\"guimenu\">Applications</"
"span>&nbsp;▸ <span class=\"guisubmenu\">Tails</span>&nbsp;▸ <span class="
"\"guimenuitem\">Supprimer le volume de stockage persistant</span></span>."

#. type: Bullet: '  1. '
msgid "Click <span class=\"guilabel\">Delete</span>."
msgstr "Cliquez sur <span class=\"guilabel\">Supprimer</span>."

#. type: Plain text
msgid ""
"This can be useful in order to delete all the files saved to the persistent "
"volume in a single operation. You can later create a new persistent volume "
"on the same USB stick without having to reinstall Tails."
msgstr ""
"Cela peut être utilisé pour supprimer en une seule opération tous les "
"fichiers sauvegardés sur le volume persistant. Vous pouvez ensuite créer un "
"nouveau volume persistant sur la même clé USB sans devoir réinstaller Tails."

#. type: Title -
#, no-wrap
msgid "Securely delete the persistent volume\n"
msgstr "Effacer le volume persistant de manière sécurisée\n"

#. type: Plain text
#, no-wrap
msgid "<div class=\"caution\">\n"
msgstr "<div class=\"caution\">\n"

#. type: Plain text
#, no-wrap
msgid ""
"<strong>The previous technique might not prevent an attacker from recovering the files in the\n"
"old persistent volume using [[data\n"
"recovery techniques|encryption_and_privacy/secure_deletion#why]].</strong>\n"
"To securely delete the persistent volume, start Tails from another USB stick or DVD, and do the following\n"
"operations on the USB stick that you want to delete securely:\n"
msgstr ""
"<strong>La procédure ci-dessus n'empêche pas un attaquant de [[récupérer les anciennes\n"
"données persistantes|encryption_and_privacy/secure_deletion#why]].</strong> Pour\n"
"plus de sécurité, démarrez Tails depuis une autre clé USB ou un DVD et suivez les opérations suivantes\n"
"sur la clé USB que vous souhaitez effacer de façon sécurisée :\n"

#. type: Plain text
#, no-wrap
msgid ""
"<ol>\n"
"<li>[[Format the USB stick and create a single encrypted partition|encryption_and_privacy/encrypted_volumes]]\n"
"on the whole USB stick. This step deletes both Tails and the persistent volume.</li>\n"
"<li>[[Securely clean all the available disk\n"
"space|encryption_and_privacy/secure_deletion#clean_disk_space]] on this new encrypted\n"
"partition.</li>\n"
"<li>[[Reinstall Tails|install]] on the USB stick.</li>\n"
"<li>Start Tails from the USB stick and [[create a new persistent\n"
"volume|persistence/configure]].</li>\n"
"</ol>\n"
msgstr ""
"<ol>\n"
"<li>[[Formatez la clé USB et créez une partition chiffrée|encryption_and_privacy/encrypted_volumes]]\n"
"sur la totalité de la clé USB. Cette étape supprime à la fois Tails et le volume persistant\n"
".</li>\n"
"<li>[[Supprimez de manière sécurisée tout l'espace disque disponible\n"
"|encryption_and_privacy/secure_deletion#clean_disk_space]] sur cette nouvelle partition\n"
"chiffrée.</li>\n"
"<li>[[Reinstallez Tails|install]] sur la clé USB.</li>\n"
"<li>Démarrez Tails depuis la clé USB et [[créez un nouveau\n"
"volume persistant|persistence/configure]].</li>\n"
"</ol>\n"

#. type: Plain text
#, no-wrap
msgid "</div>\n"
msgstr "</div>\n"

#~ msgid ""
#~ "[[!inline pages=\"doc/first_steps/persistence.caution\" raw=\"yes\"]]\n"
#~ msgstr ""
#~ "[[!inline pages=\"doc/first_steps/persistence.caution.fr\" raw=\"yes\"]]\n"
