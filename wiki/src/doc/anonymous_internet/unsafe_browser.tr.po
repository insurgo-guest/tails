# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: tails-l10n@boum.org\n"
"POT-Creation-Date: 2019-05-14 15:39+0000\n"
"PO-Revision-Date: 2018-07-02 07:17+0000\n"
"Last-Translator: emmapeel <emma.peel@riseup.net>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: tr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Weblate 2.10.1\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"Logging in to captive portals\"]]\n"
msgstr ""

#. type: Plain text
msgid ""
"Many publicly accessible Internet connections (usually available through a "
"wireless network connection) require users to first log in to a *captive "
"portal* in order to access the Internet."
msgstr ""

#. type: Plain text
msgid ""
"A captive portal is a web page that is displayed to the user before the user "
"can access the Internet. Captive portals usually require the user to log in "
"to the network or enter information such as an email address. Captive "
"portals are commonly encountered at Internet cafés, libraries, airports, "
"hotels, and universities."
msgstr ""

#. type: Plain text
msgid ""
"This is an example of a captive portal (by [AlexEng](https://commons."
"wikimedia.org/wiki/File:Captive_Portal.png)):"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[!img captive-portal.png link=\"no\" alt=\"Welcome! Please enter your credentials to connect.\"]]\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"Tor cannot be started when your Internet connection is initially blocked\n"
"by a captive portal. So, Tails includes an\n"
"<span class=\"application\">Unsafe Browser</span> to log in to captive\n"
"portals before starting Tor.\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"To start the <span class=\"application\">Unsafe Browser</span>, choose\n"
"<span class=\"menuchoice\">\n"
"  <span class=\"guimenu\">Applications</span>&nbsp;▸\n"
"  <span class=\"guisubmenu\">Internet</span>&nbsp;▸\n"
"  <span class=\"guimenuitem\">Unsafe Web Browser</span></span>.\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"The <span class=\"application\">Unsafe Browser</span> has a red theme to\n"
"differentiate it from [[<span class=\"application\">Tor Browser</span>|Tor_Browser]].\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "<div class=\"caution\">\n"
msgstr "<div class=\"caution\">\n"

#. type: Plain text
#, no-wrap
msgid ""
"<p><strong>The <span class=\"application\">Unsafe Browser</span> is not\n"
"anonymous</strong>. Use it only to log in to captive portals or to\n"
"[[browse web pages on the local network|advanced_topics/lan#browser]].</p>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "</div>\n"
msgstr "</div>\n"

#. type: Plain text
#, no-wrap
msgid "<div class=\"note\">\n"
msgstr "<div class=\"note\">\n"

#. type: Plain text
#, no-wrap
msgid "[[!inline pages=\"doc/anonymous_internet/unsafe_browser/chroot.inline\" raw=\"yes\" sort=\"age\"]]\n"
msgstr "[[!inline pages=\"doc/anonymous_internet/unsafe_browser/chroot.inline.tr\" raw=\"yes\" sort=\"age\"]]\n"

#. type: Plain text
msgid "Security recommendations:"
msgstr ""

#. type: Bullet: '* '
msgid ""
"Do not run this browser at the same time as the anonymous [[<span class="
"\"application\">Tor Browser</span>|Tor_Browser]]. This makes it easy to not "
"mistake one browser for the other, which could have catastrophic "
"consequences."
msgstr ""
