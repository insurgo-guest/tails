# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2019-06-01 11:15+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Content of: outside any tag (error?)
msgid ""
"[[!meta title=\"Donate to Tails\"]] [[!meta stylesheet=\"bootstrap.min\" rel="
"\"stylesheet\" title=\"\"]] [[!meta stylesheet=\"donate\" rel=\"stylesheet\" "
"title=\"\"]] [[!meta script=\"donate\"]]"
msgstr ""

#. type: Content of: <div><div><h2>
msgid ""
"Tails is free because nobody should have to pay to be safe while using "
"computers."
msgstr ""

#. type: Content of: <div><div><p>
msgid "Tails is used more than 25&#8239;000 times every day:"
msgstr ""

#. type: Content of: <div><div><ul><li>
msgid ""
"<b>Journalists and whistleblowers</b> use Tails to denounce the wrongdoings "
"of governments and corporations."
msgstr ""

#. type: Content of: <div><div><ul><li>
msgid ""
"<b>Human-rights defenders</b> use Tails to avoid censorship and report human-"
"rights violations."
msgstr ""

#. type: Content of: <div><div><ul><li>
msgid ""
"<b>Domestic violence survivors</b> use Tails to escape surveillance in their "
"homes."
msgstr ""

#. type: Content of: <div><div><ul><li>
msgid "<b>Privacy-concerned citizens</b> use Tails to avoid online tracking."
msgstr ""

#. type: Content of: <div><div><p>
msgid ""
"<b>But, not everyone using Tails can donate.</b> When you donate, you are "
"offering to many others who need it, the very precious tool that is Tails."
msgstr ""

#. type: Content of: <div><div><p>
msgid ""
"We know that secure tools need to be free software to be trustworthy. That's "
"why we are giving out Tails for free but ask for your help to protect and "
"sustain it."
msgstr ""

#. type: Content of: <div><div><p>
msgid ""
"Donations from individuals like you are our most valuable source of funding "
"as they make Tails more independent from government and corporate funding. "
"We are a very small non-profit and our yearly budget is ridiculously small "
"compared to the value of Tails."
msgstr ""

#. type: Content of: <div><div><p>
msgid "Please consider making a donation to protect and sustain Tails."
msgstr ""

#. type: Content of: <div><div><p>
msgid "Thanks!"
msgstr ""

#.  HTML Variables for PayPal Payments Standard:
#.        https://developer.paypal.com/docs/classic/paypal-payments-standard/integration-guide/Appx_websitestandard_htmlvariables/
#.  Note for translators: adapt the URLs to return to the page in your language.
#.  Note for translators: adapt the value of 'lc' to your language.
#.  For recurring donations only.
#.  For one-time donation only.
#. type: Content of: <div><div><div><form>
msgid ""
"<input type=\"hidden\" name=\"cmd\" value=\"_xclick-subscriptions\" id=\"cmd"
"\"/> <input type=\"hidden\" name=\"business\" value=\"tailsriseuplabs@riseup."
"net\" id=\"business\"/> <input type=\"hidden\" name=\"currency_code\" value="
"\"USD\" id=\"currency_code\"/> <input type=\"hidden\" name=\"item_name\" "
"value=\"Donation to Tails\"/> <input type=\"hidden\" name=\"no_note\" value="
"\"1\"/> <input type=\"hidden\" name=\"return\" class=\"return-url\" value="
"\"https://tails.boum.org/donate/thanks\"/> <input type=\"hidden\" name="
"\"cancel_return\" class=\"return-url\" value=\"https://tails.boum.org/donate/"
"canceled\"/> <input name=\"lc\" type=\"hidden\" value=\"US\"/> <input type="
"\"hidden\" name=\"a3\" value=\"5\" id=\"a3\"/> <input type=\"hidden\" name="
"\"t3\" value=\"M\" id=\"t3\"/> <input type=\"hidden\" name=\"p3\" value="
"\"1\"/> <input type=\"hidden\" name=\"src\" value=\"1\"/> <input type="
"\"hidden\" name=\"amount\" value=\"5\" id=\"amount\"/>"
msgstr ""

#. type: Content of: <div><div><div><form><div><div>
msgid ""
"<label class=\"btn btn-lg btn-primary active\" id=\"currency-dollar\"> "
"<input type=\"radio\" id=\"dollars\" autocomplete=\"off\" checked data-"
"complete-text=\"finished!\"/> Dollars </label> <label class=\"btn btn-lg btn-"
"primary\" id=\"currency-euro\"> <input type=\"radio\" id=\"euros\" "
"autocomplete=\"off\" checked data-complete-text=\"finished!\"/> Euros </"
"label>"
msgstr ""

#. type: Content of: <div><div><div><form><div><div>
msgid ""
"<label class=\"btn btn-lg btn-primary\" id=\"one-time\"> <input type=\"radio"
"\" autocomplete=\"off\"/> One-time </label> <label class=\"btn btn-lg btn-"
"primary active\" id=\"monthly\"> <input type=\"radio\" autocomplete=\"off\"/"
"> Monthly </label> <label class=\"btn btn-lg btn-primary\" id=\"yearly\"> "
"<input type=\"radio\" autocomplete=\"off\" checked/> Yearly </label>"
msgstr ""

#. type: Content of: <div><div><div><form><div><label>
msgid ""
"<label class=\"btn btn-amount btn-lg btn-primary col-md-3 col-xs-6 5 active"
"\"> <input type=\"radio\" autocomplete=\"off\" value=\"5\"/>$5 </label> "
"<label class=\"btn btn-amount btn-lg btn-primary col-md-3 col-xs-6 10\"> "
"<input type=\"radio\" autocomplete=\"off\" value=\"10\"/>$10 </label> <label "
"class=\"btn btn-amount btn-lg btn-primary col-md-3 col-xs-6 25\"> <input "
"type=\"radio\" autocomplete=\"off\" value=\"25\"/>$25 </label> <label class="
"\"btn btn-amount btn-lg btn-primary col-md-3 col-xs-6 50\"> <input type="
"\"radio\" autocomplete=\"off\" value=\"50\"/>$50 </label> <label class=\"btn "
"btn-amount btn-lg btn-primary col-md-3 col-xs-6 100\"> <input type=\"radio\" "
"autocomplete=\"off\" value=\"100\"/>$100 </label> <label class=\"btn btn-"
"amount btn-lg btn-primary col-md-3 col-xs-6 150\"> <input type=\"radio\" "
"autocomplete=\"off\" value=\"150\"/>$150 </label> <label class=\"btn btn-"
"amount btn-lg btn-primary col-md-3 col-xs-6 200\"> <input type=\"radio\" "
"autocomplete=\"off\" value=\"200\"/>$200 </label> <label class=\"btn btn-"
"amount btn-lg btn-primary col-md-3 col-xs-6 other\"> <span>Other</span>"
msgstr ""

#. type: Content of: <div><div><div><form><div><label><div>
msgid ""
"<span class=\"input-group-addon\">$</span> <input type=\"text\" pattern=\"\\d"
"+\" class=\"form-control\" id=\"other-dollar\"/>"
msgstr ""

#. type: Content of: <div><div><div><form><div>
msgid "</label>"
msgstr ""

#. type: Content of: <div><div><div><form><div><label>
msgid ""
"<label class=\"btn btn-amount btn-lg btn-primary col-md-3 col-xs-6 5 active"
"\"> <input type=\"radio\" autocomplete=\"off\" value=\"5\"/>5€ </label> "
"<label class=\"btn btn-amount btn-lg btn-primary col-md-3 col-xs-6 10\"> "
"<input type=\"radio\" autocomplete=\"off\" value=\"10\"/>10€ </label> <label "
"class=\"btn btn-amount btn-lg btn-primary col-md-3 col-xs-6 25\"> <input "
"type=\"radio\" autocomplete=\"off\" value=\"25\"/>25€ </label> <label class="
"\"btn btn-amount btn-lg btn-primary col-md-3 col-xs-6 50\"> <input type="
"\"radio\" autocomplete=\"off\" value=\"50\"/>50€ </label> <label class=\"btn "
"btn-amount btn-lg btn-primary col-md-3 col-xs-6 100\"> <input type=\"radio\" "
"autocomplete=\"off\" value=\"100\"/>100€ </label> <label class=\"btn btn-"
"amount btn-lg btn-primary col-md-3 col-xs-6 150\"> <input type=\"radio\" "
"autocomplete=\"off\" value=\"150\"/>150€ </label> <label class=\"btn btn-"
"amount btn-lg btn-primary col-md-3 col-xs-6 200\"> <input type=\"radio\" "
"autocomplete=\"off\" value=\"200\"/>200€ </label> <label class=\"btn btn-"
"amount btn-lg btn-primary col-md-3 col-xs-6 other\"> <span>Other</span>"
msgstr ""

#. type: Content of: <div><div><div><form><div><label><div>
msgid ""
"<input type=\"text\" pattern=\"\\d+\" class=\"form-control text-right\" id="
"\"other-euro\"/> <span class=\"input-group-addon\">€</span>"
msgstr ""

#. type: Content of: <div><div><div><form><div>
msgid ""
"<button class=\"btn btn-primary btn-lg btn-block col-md-12\">Donate</button>"
msgstr ""

#. type: Content of: <div><div><div><p>
msgid ""
"Monthly and yearly donations require a [[!img paypal-inline.png link=\"no\" "
"class=\"paypal-inline\" alt=\"PayPal\"]] account."
msgstr ""

#. type: Content of: <div><div><div><div><p>
msgid ""
"Your donation goes entirely to Tails and will be handled by RiseupLabs which "
"is a 501(c)(3) non-profit organization in the USA."
msgstr ""

#. type: Content of: <div><div><div><div><p>
msgid ""
"<strong>Donations are tax-deductible to the full extent permitted by law.</"
"strong>"
msgstr ""

#. type: Content of: <div><div><div><p>
msgid ""
"Your donation goes entirely to Tails and will be handled by CCT which is a "
"non-profit organization in Germany."
msgstr ""

#. type: Content of: <div><div><div><div><h2>
msgid "Donate in dollars"
msgstr ""

#. type: Content of: <div><div><div><div><h3>
msgid "Recurring donation"
msgstr ""

#.  Note for translators: adapt the URLs to return to the page in your language.
#.  Note for translators: adapt the value of 'lc' to your language.
#. type: Content of: <div><div><div><div><form>
msgid ""
"<input type=\"hidden\" name=\"cmd\" value=\"_xclick-subscriptions\"/> <input "
"type=\"hidden\" name=\"business\" value=\"tailsriseuplabs@riseup.net\"/> "
"<input type=\"hidden\" name=\"currency_code\" value=\"USD\" id="
"\"currency_code\"/> <input type=\"hidden\" name=\"item_name\" value="
"\"Donation to Tails\"/> <input type=\"hidden\" name=\"no_note\" value=\"1\"/"
"> <input type=\"hidden\" name=\"return\" class=\"return-url\" value="
"\"https://tails.boum.org/donate/thanks\"/> <input type=\"hidden\" name="
"\"cancel_return\" class=\"return-url\" value=\"https://tails.boum.org/donate/"
"canceled\"/> <input name=\"lc\" type=\"hidden\" value=\"US\"/> <input type="
"\"hidden\" name=\"src\" value=\"1\"/> <input type=\"hidden\" name=\"p3\" "
"value=\"1\"/> <input type=\"radio\" name=\"a3\" value=\"5\" id=\"sub5\" "
"checked=\"checked\"/><label for=\"sub5\">$5</label> <input type=\"radio\" "
"name=\"a3\" value=\"10\" id=\"sub10\"/><label for=\"sub10\">$10</label> "
"<input type=\"radio\" name=\"a3\" value=\"20\" id=\"sub20\"/><label for="
"\"sub20\">$20</label> <input type=\"radio\" name=\"a3\" value=\"50\" id="
"\"sub50\"/><label for=\"sub50\">$50</label> <input type=\"radio\" name="
"\"a3\" value=\"100\" id=\"sub100\"/><label for=\"sub100\">$100</label> "
"<input type=\"radio\" name=\"a3\" value=\"250\" id=\"sub250\"/><label for="
"\"sub250\">$250</label> <input type=\"radio\" name=\"a3\" value=\"500\" id="
"\"sub500\"/><label for=\"sub500\">$500</label>"
msgstr ""

#. type: Content of: <div><div><div><div><form>
msgid ""
"<input type=\"radio\" name=\"t3\" value=\"M\" id=\"sub_m\" checked=\"checked"
"\"/><label for=\"sub_m\">Monthly</label> <input type=\"radio\" name=\"t3\" "
"value=\"Y\" id=\"sub_y\"/><label for=\"sub_y\">Yearly</label>"
msgstr ""

#. type: Content of: <div><div><div><div><form>
msgid ""
"<input type=\"submit\" value=\"Donate\"/> (Requires a [[!img paypal-inline."
"png link=\"no\" class=\"paypal-inline\" alt=\"PayPal\"]] account.)"
msgstr ""

#. type: Content of: <div><div><div><div><h3>
msgid "One-time donation"
msgstr ""

#
#.  Note for translators: the following parts need to be translated:
#.          - https://tails.boum.org/donate/thanks
#.          - https://tails.boum.org/donate/canceled
#.          - US
#. type: Content of: <div><div><div><div><form>
msgid ""
"<input type=\"hidden\" name=\"cmd\" value=\"_donations\"/> <input type="
"\"hidden\" name='business' value=\"tailsriseuplabs@riseup.net\"/> <input "
"type=\"hidden\" name=\"currency_code\" value=\"USD\" id=\"currency_code\"/> "
"<input type=\"hidden\" name=\"item_name\" value=\"Donation to Tails\"/> "
"<input type=\"hidden\" name=\"no_note\" value=\"1\"/> <input type=\"hidden\" "
"name=\"return\" class=\"return-url\" value=\"https://tails.boum.org/donate/"
"thanks\"/> <input type=\"hidden\" name=\"cancel_return\" class=\"return-url"
"\" value=\"https://tails.boum.org/donate/canceled\"/> <input name=\"lc\" "
"type=\"hidden\" value=\"US\"/> <input type=\"radio\" name=\"amount\" value="
"\"5\" id=\"pp_5\" checked=\"checked\"/><label for=\"pp_5\">$5</label> <input "
"type=\"radio\" name=\"amount\" value=\"10\" id=\"pp_10\"/><label for="
"\"pp_10\">$10</label> <input type=\"radio\" name=\"amount\" value=\"20\" id="
"\"pp_20\"/><label for=\"pp_20\">$20</label> <input type=\"radio\" name="
"\"amount\" value=\"50\" id=\"pp_50\"/><label for=\"pp_50\">$50</label> "
"<input type=\"radio\" name=\"amount\" value=\"100\" id=\"pp_100\"/><label "
"for=\"pp_100\">$100</label> <input type=\"radio\" name=\"amount\" value=\"\" "
"id=\"pp_cust\"/><label for=\"pp_cust\">Custom amount</label>"
msgstr ""

#. type: Content of: <div><div><div><div><form>
msgid "<input type=\"submit\" value=\"Donate\"/>"
msgstr ""

#. type: Content of: <div><h3>
msgid "Other ways to donate"
msgstr ""

#. type: Content of: <div><div><div><h4>
msgid "Bitcoin"
msgstr ""

#. type: Content of: <div><div><div><div><p>
msgid ""
"<a href=\"bitcoin:1BvBMSEYstWetqTFn5Au4m4GFg7xJaNVN2\">[[!img "
"bitcoin-1BvBMSEYstWetqTFn5Au4m4GFg7xJaNVN2.png link=\"no\"]]</a>"
msgstr ""

#. type: Content of: <div><div><div><div><p>
msgid "<strong>1BvBMSEYstWetqTFn5Au4m4GFg7xJaNVN2</strong>"
msgstr ""

#. type: Content of: <div><div><div><div><p>
msgid ""
"<a href=\"bitcoin:3QirvVRntoascPfTgNTUQvKxfKwPah5FNK\">[[!img "
"bitcoin-3QirvVRntoascPfTgNTUQvKxfKwPah5FNK.png link=\"no\"]]</a>"
msgstr ""

#. type: Content of: <div><div><div><div><p>
msgid "<strong>3QirvVRntoascPfTgNTUQvKxfKwPah5FNK</strong>"
msgstr ""

#. type: Content of: <div><div><h4>
msgid "Bitcoin Cash"
msgstr ""

#. type: Content of: <div><div><p>
msgid "<strong>qrzav77wkhd942nyqvya34mya3fqxzx90ypjge0njh</strong>"
msgstr ""

#. type: Content of: <div><div><h4>
msgid "Ethereum"
msgstr ""

#. type: Content of: <div><div><p>
msgid "<strong>0xD6A73051933ab97C38cEFf2abB2f9E06F3a3ed78</strong>"
msgstr ""

#. type: Content of: <div><div><h4>
msgid "Ethereum Classic"
msgstr ""

#. type: Content of: <div><div><p>
msgid "<strong>0x86359F8b44188c105E198DdA3c0421AC60729195</strong>"
msgstr ""

#. type: Content of: <div><div><h4>
msgid "Litecoin"
msgstr ""

#. type: Content of: <div><div><p>
msgid "<strong>MJ1fqVucBt8YpfPiQTuwBtmLJtzhizx6pz</strong>"
msgstr ""

#. type: Content of: <div><div>
msgid ""
"<a id=\"other-cryptos\"></a> [[!toggle id=\"other-cryptos\" text=\"What "
"about other cryptocurrencies?\"]] [[!toggleable id=\"other-cryptos\" text="
"\"\"\""
msgstr ""

#. type: Content of: <div><div><p>
msgid ""
"Supporting a new cryptocurrency implies more work for us (learning how it "
"works, installing the software in a secure place, monitoring its value and "
"trading it, etc.). To make a good use of our time, we focus on the ones that "
"are the easiest for us to accept and the most popular."
msgstr ""

#. type: Content of: <div><div><p>
msgid ""
"Until now, cryptocurrencies other than Bitcoin represent less than 5% of our "
"donations in cryptocurrencies. We want to see cryptcurrencies other than "
"Bitcoin gain popularity before spending time on them."
msgstr ""

#. type: Content of: <div><div>
msgid "\"\"\"]]"
msgstr ""

#. type: Content of: <div><div><h4>
msgid "Bank transfer"
msgstr ""

#. type: Content of: <div><div><p>
msgid "<strong>Please make sure to mention the purpose of transfer.</strong>"
msgstr ""

#. type: Content of: <div><div><h5>
msgid "German bank account (preferred):"
msgstr ""

#. type: Content of: <div><div><div><p>
msgid "<em>Account holder</em>: Center for Cultivation of Technology gGmbH"
msgstr ""

#. type: Content of: <div><div><div><p>
msgid "<em>IBAN</em>: DE65 4306 0967 4111 9411 01"
msgstr ""

#. type: Content of: <div><div><div><p>
msgid "<em>BIC</em>: GENODEM1GLS"
msgstr ""

#. type: Content of: <div><div><div><p>
msgid "<em>Purpose of transfer</em>: TAILS-R43NGFR9"
msgstr ""

#. type: Content of: <div><div><div><p>
msgid "<em>Bank</em>: GLS Gemeinschaftsbank eG"
msgstr ""

#. type: Content of: <div><div><div><p>
msgid "<em>Bank address</em>: Christstrasse 9, 44789, Bochum, Germany"
msgstr ""

#. type: Content of: <div><div><h5>
msgid "US bank account:"
msgstr ""

#. type: Content of: <div><div><div><p>
msgid ""
"<em>Account holder</em>: TransferWise FBO Center For The Cultivation Of "
"Technology Gmbh"
msgstr ""

#. type: Content of: <div><div><div><p>
msgid "<em>Account number</em>: 8310006087"
msgstr ""

#. type: Content of: <div><div><div><p>
msgid "<em>ACH Routing number</em>: 026073150"
msgstr ""

#. type: Content of: <div><div><div><p>
msgid "<em>Wire routing number</em>: 026073008"
msgstr ""

#. type: Content of: <div><div><div><p>
msgid "<em>Bank</em>: Transferwise"
msgstr ""

#. type: Content of: <div><div><div><p>
msgid "<em>Bank address</em>: 19 W 24th Street, New York 10010, USA"
msgstr ""

#. type: Content of: <div><div><p>
msgid ""
"Your donation goes entirely to Tails and will be handled by the Center for "
"the Cultivation of Technology (CCT) which is a non-profit organization in "
"Germany."
msgstr ""

#. type: Content of: <div><div><p>
msgid ""
"<a href=\"https://techcultivation.org/donate.html\">Donations are tax-"
"deductible to the full extent permitted by law.</a>"
msgstr ""

#. type: Content of: <div><div><h4>
msgid "US check"
msgstr ""

#. type: Content of: <div><div><p>
msgid "You can send us US checks by post:"
msgstr ""

#. type: Content of: <div><div><p>
msgid "Riseup Labs"
msgstr ""

#. type: Content of: <div><div><p>
msgid "PO Box 4282"
msgstr ""

#. type: Content of: <div><div><p>
msgid "Seattle, WA 98194"
msgstr ""

#. type: Content of: <div><div><h4>
msgid "Cash"
msgstr ""

#. type: Content of: <div><div><p>
msgid "You can send us cash by post:"
msgstr ""

#. type: Content of: <div><div><p>
msgid "Weber"
msgstr ""

#. type: Content of: <div><div><p>
msgid "Merseburger Strasse 95"
msgstr ""

#. type: Content of: <div><div><p>
msgid "04177 Leipzig"
msgstr ""

#. type: Content of: <div><div><p>
msgid "Germany"
msgstr ""

#. type: Content of: <div><div><h3>
msgid "How we spend our money"
msgstr ""

#. type: Content of: <div><div><figure><figcaption><ul><li><div>
msgid "Releases & core code maintenance"
msgstr ""

#. type: Content of: <div><div><figure><figcaption><ul><li><div>
msgid "(22%)"
msgstr ""

#. type: Content of: <div><div><figure><figcaption><ul><li><div>
msgid "Administration & management"
msgstr ""

#. type: Content of: <div><div><figure><figcaption><ul><li><div>
msgid "New features"
msgstr ""

#. type: Content of: <div><div><figure><figcaption><ul><li><div>
msgid "(21%)"
msgstr ""

#. type: Content of: <div><div><figure><figcaption><ul><li><div>
msgid "Infrastructure"
msgstr ""

#. type: Content of: <div><div><figure><figcaption><ul><li><div>
msgid "(10%)"
msgstr ""

#. type: Content of: <div><div><figure><figcaption><ul><li><div>
msgid "Help desk"
msgstr ""

#. type: Content of: <div><div><figure><figcaption><ul><li><div>
msgid "Meetings"
msgstr ""

#. type: Content of: <div><div><figure><figcaption><ul><li><div>
msgid "User experience & documentation"
msgstr ""

#. type: Content of: <div><div><figure><figcaption><ul><li><div>
msgid "(5%)"
msgstr ""

#. type: Content of: <div><div><p>
msgid "Our financial records are available [[here|doc/about/finances]]."
msgstr ""

#. type: Content of: <div><div><h3>
msgid "Our partners"
msgstr ""

#. type: Content of: <div><div><p>
msgid ""
"Tails also receives grants, awards, corporate donations, and substantial "
"donations from individuals."
msgstr ""

#. type: Content of: <div><div><p>
msgid "Meet our [[partners|partners]] or [[become a partner|partners/become]]!"
msgstr ""
